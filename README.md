# Projet IMSP (Kiruban PREMKUMAR, Adrien BROYERE)

## 2ème PARTIE

Le fait de réserver deux ressources provoque des interblocages de processus et le programme s'arrête.

Exemple :

```
	Soit 3 processus A,B,C et 3 ressources R1, R2 et R3

	- A reserve R1, B reserve R2, C reserve R3
	- A attends la libération d'une ressource
	- B attends la libération d'une ressource
	- C attends la libération d'une ressource
	
	Conclusion => Interblocages de processus. (deadlocks)
```