#define NBP 4
#define NBU 3
#include "semaphoresAF.pml"

byte S[NBU] = 0;
sema sem, attente;


inline reserver(i,r,r2)
{
	P(sem);
	byte j = 0 ;
	byte nb = 0;
	do
	:: (nb == 2) -> 
		S[r] = i;
		S[r2] = i;
		break;
	:: (j == NBU) -> 
		assert(j == NBU);
		if
		:: (nb == 1) -> S[r] = 0;
		:: else -> skip;
		fi;
		V(sem);
		P(attente);
		P(sem);
		j = 0;
		nb = 0;
	:: (nb != 2 && j < NBU) -> 
		if
		:: (S[j] == 0) -> 
			assert(S[j] == 0);
			if
			:: (nb == 0) -> r = j;
			:: (nb == 1) -> r2 = j;
			:: else -> skip;
			fi;
			nb++;
		:: else -> 
			skip;
		fi;
		j++;
	od;
	V(sem);
}

inline utiliser(r,r2)
{
	printf("DEBUT d'utilisation des ressources {%d,%d}\n",r,r2);
	printf("FIN d'utilisation des ressources {%d,%d}\n",r,r2);
}

inline liberer(i)
{
	P(sem);
	byte k = 0;
	do
	:: (k == NBU) ->
		assert(k == NBU);
		break;
	:: (k < NBU) -> 
		assert(k < NBU);
		if
		:: (S[k] == i) -> 
			assert(S[k] == i);
			S[k] = 0;
		    V(attente);
		:: else -> 
			skip;
		fi;
		k++;
	od;
	V(sem);
}

proctype utilisateur(byte i)
{	
	byte r,r2;
	do
	:: true ->
		printf("DEBUT processus %d\n",i);
		reserver(i,r,r2);
		utiliser(r,r2);
		liberer(i);
		assert(r <= NBU);
		assert(r2 <= NBU);
		assert(i <= NBP);
		printf("FIN processus %d\n",i);
	od;
}


init {
	byte i = 0;
	initialise(sem,1);
	initialise(attente,0);
	atomic {
		do
		:: (i == NBP) -> 
			break;
		:: (i < NBP) -> 
			i++;
			run utilisateur(i);
		od;
	}
}