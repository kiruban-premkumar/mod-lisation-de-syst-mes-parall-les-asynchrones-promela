typedef sema {
   byte cpt;
   chan liste =[NBP] of {byte};
}

inline initialise (s,val)
   {s.cpt=val}

inline P(s){
   atomic{
      if
      :: (s.cpt>0)->s.cpt--;
      :: else s.liste!_pid; 
         (s.cpt>0) && s.liste?[eval(_pid)] ;
         s.liste?_pid;
         s.cpt--;
      fi
   }
}


inline V(s)
   {s.cpt++}