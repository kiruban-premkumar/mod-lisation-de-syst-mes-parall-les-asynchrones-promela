#define NBP 4
#define NBU 3
#include "semaphoresAF.pml"

byte S[NBU] = 0;
sema sem, attente;


inline reserver(i,r)
{
	P(sem);
	r = 0 ;
	do
	:: (r == NBU) -> 
		assert(r == NBU);
		V(sem);
		P(attente);
		P(sem);
		r = 0;
	:: (r < NBU) -> 
		if
		:: (S[r] == 0) -> 
			assert(S[r] == 0);
			S[r] = i; 
			break;
		:: else -> 
			skip;
		fi;
		r++;
	od;
	V(sem);
}

inline utiliser(r)
{
	printf("DEBUT d'utilisation de la ressource %d\n",r);
	printf("FIN d'utilisation de la ressource %d\n",r);
}

inline liberer(i)
{
	P(sem);
	r = 0;
	do
	:: (r == NBU) -> 
		assert(r == NBU);
		break;
	:: (r < NBU) -> 
		assert(r < NBU);
		if
		:: (S[r] == i) -> 
			assert(S[r] == i);
			S[r] = 0;
			V(attente);
			break;
		:: else -> 
			skip;
		fi;
		r++;
	od;
	V(sem);
}

proctype utilisateur(byte i)
{	
	do
	:: true ->
		byte r;
		printf("DEBUT processus %d\n",i);
		reserver(i,r);
		reserver(i,r);
		utiliser(r);
		liberer(i);
		assert(r <= NBU);
		assert(i <= NBP);
		printf("FIN processus %d\n",i);
	od;
}


init {
	byte i = 0;
	initialise(sem,1);
	initialise(attente,0);
	atomic {
		do
		:: (i == NBP) -> 
			break;
		:: (i < NBP) -> 
			i++;
			run utilisateur(i);
		od;
	}
}
